// JS
import './js/vertical-carousel';
import './js/start-screen';
import './js/navigation';
import './js/product-desc';
import './js/get-template';
import './js/cookies';

// Custom SCSS
import './css/styles.scss';

// Images
import './img/components/start-screen/start-screen-bg.jpg';
import './img/components/start-screen/rei-logo.png';
import './img/products/ang-2200.jpg';
import './img/products/andre.jpg';
import './img/products/orion-900-hx.jpg';
import './img/products/orion-2-4-hx.jpg';
import './img/products/orion-hx-deluxe.jpg';
import './img/components/about-rei/about-rei.jpg';
import './img/components/contact/mail.svg';
import './img/components/contact/phone-call.svg';
import './img/components/contact/contact-bg.jpg';