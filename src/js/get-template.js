/**
 * @author Maciej Wójcik <wojcik.maciej@outlook.com>
 */

(function ($) {

    var Template = {

        load: function (name) {
            $.get('/tpl/' + name + '.html', function (data) {
                Template.insert(name, data);
            }).fail(function () {
                console.log('Template content cannot be loaded');
            });
        },

        insert: function (name, data) {
            $('#' + name + ' .modal-body').html(data);
        },

    }

    $(document).ready(function () {
        $('body').on('click', '.get-template', function (event) {
            event.preventDefault();

            $('#' + event.target.dataset.template).on('show.bs.modal', function (event) {
                Template.load(event.target.id);
            });

            $('#' + event.target.dataset.template).modal('toggle');
        });

    });

}(jQuery));