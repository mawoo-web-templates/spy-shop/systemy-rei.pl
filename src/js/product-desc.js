/**
 * @author Maciej Wójcik <wojcik.maciej@outlook.com>
 */

import config from './config';
import viewport from './viewport';

(function ($) {
    var ProductDesc = {

        productHeader: $('.product__header > h2'),

        productDesc: $('.product__desc'),

        reset: function () {
            this.productDesc.removeAttr('style');
        },

        setProductDescriptionPosition: function () {
            this.reset();

            this.productHeader.each(function () {
                var productHeaderTopPos = $(this).position().top,
                    productHeaderHeight = $(this).outerHeight(),
                    padding = 60;

                $(this).parent().next().css({
                    'padding-top': productHeaderTopPos + productHeaderHeight + padding
                });
            });
        },

        init: function () {
            if (viewport.width() >= config.breakpoint) {
                this.setProductDescriptionPosition();
            } else {
                this.reset();
            }
        }

    };

    $(document).ready(function () {
        ProductDesc.init();
    });

    $(window).on('resize', function () {
        ProductDesc.init();
    });

}(jQuery));