/**
 * @author Maciej Wójcik <wojcik.maciej@outlook.com>
 */

module.exports = {

    status: true,

    carouselItemsStatus: [],

    carouselItemsAddStatus: function () {
        var carouselItemsLength;

        if (this.status) {
            carouselItemsLength = document.querySelectorAll('.vertical-carousel__item').length;

            for (var i = 0; i < carouselItemsLength; i++) {
                this.carouselItemsStatus.push(true);
            }

            this.status = false;
        }
    },

    dataLayerObject: function (index) {
        var id = document.querySelectorAll('.vertical-carousel__item')[index].id,
            obj = {
                'sekcja': id,
                'event': 'przewijanie'
            };

        return obj;
    },

    addObjectToDataLayer: function (index, callback) {
        if (this.carouselItemsStatus[index]) {
            dataLayer.push(callback(index));
            //console.log(dataLayer);
            this.carouselItemsStatus[index] = false;
        }
    },

    init: function (index) {

        this.carouselItemsAddStatus();

        this.addObjectToDataLayer(index, this.dataLayerObject);

    }

}