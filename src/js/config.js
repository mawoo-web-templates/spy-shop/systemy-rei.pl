/**
 * @author Maciej Wójcik <wojcik.maciej@outlook.com>
 * @module config
 */

module.exports = {

    breakpoint: 1200,
    carouselItem: $('.vertical-carousel__item'),
    carouselItems: $('.vertical-carousel__items'),
    navigationItem: $('.navigation__item'),

}