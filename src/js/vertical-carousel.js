/**
 * @author Maciej Wójcik <wojcik.maciej@outlook.com>
 */

import config from './config';
import viewport from './viewport';
import GTMDataLayer from './gtm-data-layer';

(function ($) {
    var VerticalCarousel = {

        translateStatus: true,

        modalStatus: function () {
            return $('body').hasClass('modal-open')
        },

        cachedWindowWidth: viewport.width(),

        sectionLength: function () {
            return config.carouselItem.length;
        },

        activeItemIndex: function () {
            return config.carouselItem.parent().find('.active').index();
        },

        checkDevice: function () {
            this.events.desktop.remove();
            this.events.mobile.remove();

            if (viewport.width() >= config.breakpoint) {
                if (!('ontouchstart' in document.documentElement)) {
                    this.adjustSectionHeight();
                    this.events.desktop.add();
                }
            } else {
                this.events.mobile.add();
            }
        },

        // Desktop
        events: {
            desktop: {
                add: function () {
                    window.addEventListener('wheel', VerticalCarousel.initTranslate, false);
                },
                remove: function () {
                    window.removeEventListener('wheel', VerticalCarousel.initTranslate, false);
                }
            },
            mobile: {
                add: function () {
                    window.addEventListener('scroll', VerticalCarousel.checkItemVisibility, false);
                },
                remove: function () {
                    window.addEventListener('scroll', VerticalCarousel.checkItemVisibility, false);
                }
            }
        },

        adjustSectionHeight: function () {
            var newWidth = $(window).width();
            if (newWidth !== this.cachedWindowWidth) {
                config.carouselItem.height(viewport.height());
                this.cachedWindowWidth = newWidth;
            } else {
                config.carouselItem.height(viewport.height());
            }
        },

        initTranslate: function (event) {
            if (VerticalCarousel.translateStatus && !VerticalCarousel.modalStatus()) {
                if (event.deltaY > 0 && VerticalCarousel.activeItemIndex() !== (VerticalCarousel.sectionLength() - 1)) {
                    VerticalCarousel.moveDown();
                    VerticalCarousel.translateStatus = false;
                } else if (event.deltaY < 0 && VerticalCarousel.activeItemIndex() !== 0) {
                    VerticalCarousel.moveUp();
                    VerticalCarousel.translateStatus = false;
                }
            }
        },

        moveDown: function () {
            var nextItemIndex = this.activeItemIndex() + 1;
            var value = 0 - (nextItemIndex * viewport.height());

            this.translate(value, config.carouselItem, nextItemIndex);
        },

        moveUp: function () {
            var prevItemIndex = this.activeItemIndex() - 1;
            var value = 0 - (prevItemIndex * viewport.height());

            this.translate(value, config.carouselItem, prevItemIndex);
        },

        translate: function (value, item, index) {
            config.navigationItem.removeClass('active');
            config.carouselItem.eq(this.activeItemIndex()).removeClass('active');
            config.carouselItems.css({
                transform: 'translateY(' + value + 'px)'
            });

            setTimeout(function () {
                VerticalCarousel.translateStatus = true;
                config.navigationItem.eq(index).addClass('active');
                item.eq(index).addClass('active');

                // Google Tag Manager dataLayer array
                GTMDataLayer.init(index);
            }, 600);
        },

        itemPositionToTheViewport: function () {
            var items = document.getElementsByClassName('vertical-carousel__item'),
                itemsLength = items.length,
                arr = [];

            for (var i = 0; i < itemsLength; i++) {
                arr.push(items[i].getBoundingClientRect().top);
            }

            return arr;
        },

        checkItemVisibility: function () {
            var arr = VerticalCarousel.itemPositionToTheViewport(),
                arrLength = arr.length;

            for (var index = 0; index < arrLength; index++) {
                if (arr[index] <= 0 && arr[index] >= (0 - viewport.height())) {
                    // Google Tag Manager dataLayer array
                    GTMDataLayer.init(index);
                }
            }
        },

        reset: function () {
            config.carouselItems.removeAttr('style');
            config.carouselItem.removeAttr('style');
            config.carouselItem.removeClass('active').eq(0).addClass('active');
            config.navigationItem.removeClass('active').eq(0).addClass('active');
        },

        init: function () {
            this.checkDevice();
        }

    };

    $(document).ready(function () {
        VerticalCarousel.init();

        // Google Tag Manager dataLayer array
        GTMDataLayer.init(0);

        $(window).on('resize', function () {
            VerticalCarousel.reset();
            VerticalCarousel.init();
        });
    });
}(jQuery));