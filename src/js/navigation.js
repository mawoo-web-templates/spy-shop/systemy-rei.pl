/**
 * @author Maciej Wójcik <wojcik.maciej@outlook.com>
 */

import config from './config';
import viewport from './viewport';
import GTMDataLayer from './gtm-data-layer';

(function ($) {
    var Navigation = {

        toggleMenuStatus: false,

        animateStatus: true,

        activeItemIndex: function () {
            return config.carouselItem.parent().find('.active').index();
        },

        menuWidth: function () {
            return $('#navigation-collapse').outerWidth();
        },

        animate: function (hash, el) {

            if (this.animateStatus) {
                this.animateStatus = false;
                config.navigationItem.removeClass('active');

                if (viewport.width() < config.breakpoint) {

                    $('html, body').animate({
                        scrollTop: $(hash).offset().top - 50
                    }, 600, 'easeInOutQuart', function () {
                        Navigation.animateStatus = true;
                        $(el).parent().addClass('active');
                        $(this).clearQueue();
                    });

                    this.toggleMenu();

                } else if (viewport.width() >= config.breakpoint) {

                    var hashIndex = $(hash).index();
                    var value = 0 - (hashIndex * viewport.height());
                    var item = config.carouselItem;

                    config.carouselItem.eq(this.activeItemIndex()).removeClass('active');
                    config.carouselItems.css({
                        transform: 'translateY(' + value + 'px)'
                    });

                    setTimeout(function () {
                        Navigation.animateStatus = true;
                        config.navigationItem.eq(hashIndex).addClass('active');
                        item.eq(hashIndex).addClass('active');

                        // Google Tag Manager dataLayer array
                        GTMDataLayer.init(hashIndex);
                    }, 600);

                }
            }
        },

        toggleMenu: function () {
            if (this.toggleMenuStatus) {
                this.toggleMenuStatus = false;
                $('#navigation-collapse, #navigation-toggler, #vertical-carousel').removeClass('expanded-menu');
            } else {
                this.toggleMenuStatus = true;
                $('#navigation-collapse, #navigation-toggler, #vertical-carousel').addClass('expanded-menu');
            }
        }
    };

    $(document).ready(function () {
        $('#navigation').on('click', '#navigation-toggler', function () {
            Navigation.toggleMenu();
        });

        $('#navigation').on('click', '.navigation__link', function (event) {
            event.preventDefault();
            var hash = event.target.hash;

            Navigation.animate(hash, $(this));
        });
    });

}(jQuery));