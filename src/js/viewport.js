/**
 * @author Maciej Wójcik <wojcik.maciej@outlook.com>
 * @module viewport
 */

module.exports = {

    height: function () {
        return $(window).height();
    },

    width: function () {
        return $(window).width();
    }

}