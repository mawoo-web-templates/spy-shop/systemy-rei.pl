/**
 * @author Maciej Wójcik <wojcik.maciej@outlook.com>
 */

import config from './config';
import viewport from './viewport';

(function ($) {
    var StartScreen = {

        element: $('#start-screen'),

        init: function () {
            if (config.breakpoint > viewport.width()) {
                this.element.height(viewport.height());
            } else {
                this.element.removeAttr('style');
            }
        }
    };

    $(document).ready(function () {
        StartScreen.init();

        $(window).on('resize', function () {
            StartScreen.init();
        });
    });

}(jQuery));