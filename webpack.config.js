const path = require('path');
const ExtractTextPlugin = require("extract-text-webpack-plugin"); // ExtractTextPlugin
const HtmlWebpackPlugin = require('html-webpack-plugin'); // HtmlWebpackPlugin
const UglifyJsPlugin = require('uglifyjs-webpack-plugin') // UglifyjsWebpackPlugin
const CopyWebpackPlugin = require('copy-webpack-plugin') // CopyWebpackPlugin

module.exports = {
    entry: './src/index.js',
    output: {
        filename: 'js/app.js',
        path: path.resolve(__dirname, 'dist')
    },
    module: {
        rules: [{
            test: /\.scss$/,
            use: ExtractTextPlugin.extract({
                use: [{
                    loader: 'css-loader',
                    options: {
                        minimize: true,
                        sourceMap: true,
                        url: false
                    }
                }, {
                    loader: 'postcss-loader',
                    options: {
                        sourceMap: true
                    }
                }, {
                    loader: 'sass-loader',
                    options: {
                        sourceMap: true
                    }
                }],
                // use style-loader in development
                fallback: "style-loader"
            })
        }, {
            test: /\.(gif|png|jpe?g|svg)$/i,
            use: [{
                loader: 'file-loader',
                options: {
                    name: '[name].[ext]',
                    outputPath: 'img/'
                }
            }, {
                loader: 'image-webpack-loader',
                options: {
                    bypassOnDebug: true,
                }
            }]
        }]
    },
    plugins: [
        new ExtractTextPlugin({
            filename: "css/[name].[contenthash:8].css"
        }),
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: './src/index.html',
            minify: {
                collapseWhitespace: true,
                minifyCSS: true,
                minifyJS: true,
                removeComments: true
            }
        }),
        new HtmlWebpackPlugin({
            filename: 'tpl/terms-and-conditions.html',
            template: './src/tpl/terms-and-conditions.html',
            inject: false,
            minify: {
                collapseWhitespace: true,
                minifyCSS: true,
                minifyJS: true,
                removeComments: true
            }
        }),
        new UglifyJsPlugin({
            sourceMap: true
        }),
        new CopyWebpackPlugin([{
            from: './src/googlecf68af412099da49.html'
        }, {
            from: './src/.htaccess'
        }]),
    ],
    devServer: {
        contentBase: path.join(__dirname, "dist"),
        compress: true,
        port: 9000
    },
    devtool: 'source-map',
    watch: true
};